﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FuncoesMatematicas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Abs = " + Math.Abs(-67));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ceiling = " + Math.Ceiling(67.7));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Floor = " + Math.Floor(-67.7));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Min = " + Math.Min(10,2));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Max = " + Math.Max(10,7));
        }

        private void button6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pow = " + Math.Pow(2, 2));
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //raiz quadrada
            MessageBox.Show("SQRT = " + Math.Sqrt(49));
        }

        private void button8_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Round = " + Math.Round(1.8));
        }
    }
}
